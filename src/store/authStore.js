import { observable, action, toJS, when, computed } from "mobx";
import { URL, AUTH_BASIC_LOGIN, AUTH_BASIC_PASS } from "../constants";
import {
  setShowAuth,
  setShowAlert,
  setAlertMessage,
  setShowRegisterAgeAndGender,
} from "./uiStore";

const setAccessToken = action((token) => accessToken.set(token));
const setRefreshToken = action((token) => refreshToken.set(token));
const setLogin = action((l) => login.set(l));
const setPassword = action((p) => password.set(p));
const setGender = action((g) => gender.set(g));
const setAge = action((a) => {
  if (a < 0 || age > 100) age.set(18);
  else age.set(a);
});
const setLikes = action((userLikes) => {
  while (likes.length > 0) likes.pop();
  userLikes.forEach((like) => likes.push(like));
});

export const accessToken = observable.box("");
const refreshToken = observable.box("");
export const isLoggedIn = computed(
  () => accessToken.get() !== "" && refreshToken.get() !== ""
);

when(
  () => accessToken.get() === "" && refreshToken.get() === "",
  () => {
    const tokens = JSON.parse(localStorage.getItem("tokens"));
    if (tokens === null) return;
    const { access_token, refresh_token } = tokens;
    setAccessToken(access_token);
    setRefreshToken(refresh_token);
  }
);

const logout = () => {
  setAccessToken("");
  setRefreshToken("");
  setLogin("");
  setPassword("");
  setLikes([]);
  const isOld = JSON.parse(localStorage.getItem("isOld")) || false;
  localStorage.clear();
  if (isOld) localStorage.setItem("isOld", JSON.stringify(isOld));
};

export const login = observable.box("");
export const password = observable.box("");
const gender = observable.box(true);
const age = observable.box(18);

export const resetLoginAndPassword = action(() => {
  setLogin("");
  setPassword("");
});

const likes = observable([]);
const appendLikes = action((likes) => {
  const copy = toJS(likes).slice();
  setLikes([...copy, ...likes]);
});
const toggleLike = action((hubId) => {
  const copy = toJS(likes).slice();
  const like = copy.find(({ hubId: id }) => id === hubId);
  if (like === undefined) {
    copy.push({ hubId });
    setLikes(copy);
  } else {
    setLikes(copy.filter(({ hubId: id }) => id !== hubId));
  }
});

export const getUserLikes = action((page = 0) => {
  fetch(`${URL}/core/like/getUserLikes?page=${page}`, {
    method: "GET",
    headers: {
      Authorization: `Bearer ${accessToken.get()}`,
    },
  })
    .then((response) => {
      if (response.ok) return response.json();
      else throw new Error(response.status);
    })
    .then(({ response: userLikes = [] }) => {
      return userLikes.map(({ hubId }) => ({ hubId }));
    })
    .then((likes) => {
      if (likes.length === 0) return;
      else {
        appendLikes(likes);
        getUserLikes(page + 1);
      }
    })
    .catch(({ message: status }) => {
      if (status === "401") {
        if (accessToken.get() !== "" && refreshToken.get() !== "") {
          reauth().then((response) => {
            if (response === 200) getUserLikes();
          });
        }
      }
    });
});

when(
  () => likes.length === 0,
  () => {
    getUserLikes(0);
  }
);

export const checkIfUserExists = async () => {
  const nick = login.get();
  const exists = await fetch(`${URL}/core/user/checkIfExists?nick=${nick}`)
    .then((response) => response.json())
    .then(({ response: exists }) => exists)
    .catch((error) => console.error(error));
  return exists;
};

const register = () => {
  fetch(`${URL}/core/user/signup`, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify({
      nick: login.get(),
      password: password.get(),
      age: Number(age.get()),
      gender: Number(gender.get()),
    }),
  })
    .then((response) => response.json())
    .then(() => auth())
    .catch((error) => console.error(error));
};

export const auth = () => {
  const body = `grant_type=password&username=${encodeURIComponent(
    login.get()
  )}&password=${encodeURIComponent(password.get())}`;
  fetch(`${URL}/oauth/token`, {
    method: "POST",
    headers: {
      "Content-Type": "application/x-www-form-urlencoded",
      Authorization: `Basic ${btoa(`${AUTH_BASIC_LOGIN}:${AUTH_BASIC_PASS}`)}`,
    },
    body: body,
  })
    .then((response) => {
      if (response.ok) {
        return response.json();
      } else {
        throw new Error(response.status);
      }
    })
    .then((response) => {
      localStorage.setItem("isOld", JSON.stringify(true));
      localStorage.setItem(
        "user",
        JSON.stringify({
          login: login.get(),
          age: age.get(),
          gender: gender.get(),
        })
      );
      const { access_token, refresh_token } = response;
      setAccessToken(access_token);
      setRefreshToken(refresh_token);
      localStorage.setItem(
        "tokens",
        JSON.stringify({ access_token, refresh_token })
      );
      setShowAuth(false);
      setShowRegisterAgeAndGender(false);
      getUserLikes(0);
    })
    .catch(({ message: code }) => {
      switch (code) {
        case "400":
          setAlertMessage("Неправильный логин или пароль");
          setShowAlert(true);
          break;
        default:
          console.error(code);
      }
    });
};

export const reauth = async () => {
  const body = `grant_type=refresh_token&refresh_token=${refreshToken.get()}`;
  return await fetch(`${URL}/oauth/token`, {
    method: "POST",
    headers: {
      "Content-Type": "application/x-www-form-urlencoded",
      Authorization: `Basic ${btoa(`${AUTH_BASIC_LOGIN}:${AUTH_BASIC_PASS}`)}`,
    },
    body: body,
  })
    .then((response) => {
      if (response.ok) return response.json();
      else {
        throw new Error(response.status);
      }
    })
    .then((response) => {
      const { access_token, refresh_token } = response;
      setAccessToken(access_token);
      setRefreshToken(refresh_token);
      localStorage.setItem(
        "tokens",
        JSON.stringify({ access_token, refresh_token })
      );
      setShowAuth(false);
      return 200;
    })
    .catch((error) => {
      setShowAuth(true);
      setAccessToken("");
      setRefreshToken("");
      return error.message;
    });
};

export default {
  accessToken,
  refreshToken,
  isLoggedIn,
  login,
  password,
  gender,
  age,
  likes,
  resetLoginAndPassword,
  setAccessToken,
  setRefreshToken,
  setLogin,
  setPassword,
  setGender,
  setAge,
  getUserLikes,
  checkIfUserExists,
  auth,
  reauth,
  register,
  toggleLike,
  logout,
};
