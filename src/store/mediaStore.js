import { observable, action, autorun, when, toJS, reaction } from "mobx";
import axios from "axios";
import { URL } from "../constants";
import { accessToken, reauth } from "./authStore";
import { setShowCreateHub } from "./uiStore";
import { observer } from "mobx-react";

const setCurrentCategory = action((category) => currentCategory.set(category));
const setCategories = action((cs) => {
  while (categories.length > 0) categories.pop();
  cs.forEach((category) => categories.push(category));
});
const setHubs = action((updHubs) => {
  while (hubs.length > 0) hubs.pop();
  updHubs.forEach((hub) => hubs.push(hub));
});
const setNewHubContent = action((content) => {
  newHubContent.set(content);
});
const setNewHubCategory = action((category) => newHubCategory.set(category));
const setNewCommentContent = action((comment) =>
  newCommentContent.set(comment)
);

/**  ----------------------------------------------  */

const currentFilter = observable.box("smartLikesAndComment");
const setCurrentFilter = action((filter) => currentFilter.set(filter));

const categoryDescription = observable.box("");
const setCategoryDescription = action((description) =>
  categoryDescription.set(description)
);
const categoryImage = observable.box("");
const setCategoryImage = action((image) => categoryImage.set(image));

const hasMore = observable.box(false);
export const setHasMore = action((more) => hasMore.set(more));

const currentCategory = observable.box("");
const categories = observable([]);

const newHubContent = observable.box("");
const newHubCategory = observable.box(currentCategory.get());

const newCommentContent = observable.box("");

const hubs = observable([]);
const appendHub = action((hub) => {
  const copy = toJS(hubs).slice();
  copy.push(hub);
  setHubs(copy);
});
const appendHubs = action((append) => {
  setHubs([...toJS(hubs).slice(), ...append]);
});
const resetHubs = () => setHubs([]);
const getHubById = (id) => toJS(hubs).find(({ hubId }) => hubId === id);

const toggleComments = action((hubId) => {
  const copy = toJS(hubs).slice();
  const hub = copy.find(({ hubId: id }) => hubId === id);
  hub.showComments = !hub.showComments;
  setHubs(copy);
});

const fetchComments = (page, hubId = "0") => {
  return axios({
    method: "GET",
    url: `${URL}/core/comment/getComments`,
    params: { page, hubId },
  })
    .then((response) => response.data)
    .then(({ response = [] }) => response)
    .catch((error) => console.log(error));
};

const fetchHubs = (page = 0, sort = "smartLikesAndComment") =>
  axios({
    method: "GET",
    url: `${URL}/core/hub/getHubs`,
    params: { page, sort },
  })
    .then((response) => response.data)
    .then(({ response: hubs = [] }) =>
      hubs.map((hub) =>
        Object.assign(
          {},
          hub,
          { showComments: false },
          { comments: hub.comments === null ? [] : hub.comments }
        )
      )
    )
    .catch((error) => console.error(error));

const fetchHubsWithCategories = (page = 0, sort = "smartLikesAndComment") => {
  const categories = currentCategory.get();
  return axios({
    method: "GET",
    url: `${URL}/core/hub/getHubsWithCategoriesIn`,
    params: { page, sort, categories },
  })
    .then((response) => response.data)
    .then(({ response: hubs = [] }) =>
      hubs.map((hub) => Object.assign({}, hub, { showComments: false }))
    )
    .catch((error) => console.error(error));
};

when(
  () => categories.length === 0,
  () =>
    fetch(`${URL}/core/hubcategory/getCategories?page=0`)
      .then((response) => {
        if (response.ok) {
          return response.json();
        } else {
          throw response.json();
        }
      })
      .then(({ response = [] }) => {
        setCategories(response);
      })
      .catch((error) => console.log(error))
);

const like = (hubId) => {
  fetch(`${URL}/core/like/toggle?hubId=${hubId}`, {
    method: "POST",
    headers: {
      Authorization: `Bearer ${accessToken.get()}`,
    },
  })
    .then((response) => {
      if (response.ok) {
      } else {
        throw new Error(response.status);
      }
    })
    .catch(({ message: status }) => {
      if (status === "401") {
        reauth().then((response) => {
          if (response.ok) like(hubId);
        });
      }
    });
};

const createPost = async () => {
  const content = newHubContent.get();
  let newHubCategoryJS = newHubCategory.get();
  if (newHubCategoryJS === "all")
    newHubCategoryJS = toJS(categories)[0].category;

  const category = toJS(categories).find(
    ({ category }) => category === newHubCategoryJS
  );
  const body = { content, categories: category.category };
  return await fetch(`${URL}/core/hub/create`, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
      Authorization: `Bearer ${accessToken.get()}`,
    },
    body: JSON.stringify(body),
  })
    .then((response) => {
      if (response.ok) return response.json();
      else throw new Error(response.status);
    })
    .then(({ response }) => {
      appendHub(response);
      setShowCreateHub(false);
      setNewHubContent("");
      return response;
    })
    .catch(({ messege: status }) => {
      if (status === "401") {
        reauth().then((response) => {
          if (response.ok) return createPost();
        });
      }
    });
};

const appendedComments = observable([]);
const setAppendedComments = action((comments) => {
  while (appendedComments.length > 0) appendedComments.pop();
  comments.forEach((comment) => appendedComments.push(comment));
});
const incrementComments = (hubId) => {
  const copy = toJS(appendedComments).slice();
  const comments = copy.find(({ hubId: id }) => id === hubId) || {};
  comments.appended++;
  setAppendedComments(copy);
};
autorun(() => {
  const comments = toJS(hubs).map((hub) => {
    const comments = toJS(appendedComments).find(
      ({ hubId }) => hubId === hub.hubId
    );
    return { hubId: hub.hubId, appended: comments ? comments.appended : 0 };
  });
  setAppendedComments(comments);
});

const createComment = (hubId, setter) => {
  const body = { hubId, content: newCommentContent.get() };
  fetch(`${URL}/core/comment/create`, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
      Authorization: `Bearer ${accessToken.get()}`,
    },
    body: JSON.stringify(body),
  })
    .then((response) => {
      if (response.ok) return response.json();
      else {
        response.json().then((response) => console.log(response));
        throw new Error(response.status);
      }
    })
    .then(({ response }) => {
      setter((prev) => [...prev, response]);
      incrementComments(hubId);
      setNewCommentContent("");
    })
    .catch(({ message: status }) => {
      if (status === "401") {
        reauth().then((response) => {
          if (response === 200) {
            createComment(hubId);
          }
        });
      }
    });
};

export default {
  currentCategory,
  setCurrentCategory,
  categories,
  hubs,
  setHubs,
  appendHubs,
  resetHubs,
  fetchHubs,
  fetchHubsWithCategories,
  fetchComments,
  getHubById,
  toggleComments,
  like,
  setNewHubContent,
  setNewHubCategory,
  newHubContent,
  newHubCategory,
  createPost,
  newCommentContent,
  setNewCommentContent,
  createComment,
  hasMore,
  setHasMore,
  appendedComments,
  currentFilter,
  setCurrentFilter,
  categoryDescription,
  setCategoryDescription,
  categoryImage,
  setCategoryImage,
};
