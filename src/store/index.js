import mediaStore from "./mediaStore";
import uiStore from "./uiStore";
import authStore from "./authStore";

export default {
  mediaStore,
  uiStore,
  authStore,
};
