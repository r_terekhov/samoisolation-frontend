import { observable, action, autorun, toJS } from "mobx";
import mediaStore from "./mediaStore";
import {
  login,
  password,
  auth,
  resetLoginAndPassword,
  checkIfUserExists,
} from "./authStore";

const setShowSpinner = action((show) => showSpinner.set(show));
export const setShowAlert = action((show) => showAlert.set(show));
export const setAlertMessage = action((msg) => alertMessage.set(msg));
export const setShowAuth = action((show) => {
  showAuth.set(show);
  if (show === false) {
    setShowAlert(false);
    resetLoginAndPassword();
  }
});
export const setShowRegisterAgeAndGender = action((show) =>
  showRegisterAgeAndGender.set(show)
);
export const setShowCreateHub = action((show) => showCreateHub.set(show));

/**  ---------------------------------------------------  */

const toggleComments = (hubId) => {
  const hubs = toJS(mediaStore.hubs).slice();
  const hub = hubs.find(({ hubId: id }) => id === hubId);
  hub.showComments = true;
  mediaStore.setHubs(hubs);
};

const showSpinner = observable.box(false);
const showAlert = observable.box(false);
const alertMessage = observable.box("");

const showAuth = observable.box(false);
const showRegisterAgeAndGender = observable.box(false);
const showCreateHub = observable.box(false);

const authorize = () => {
  const errors = [];
  if (login.get() === "") errors.push("Введите логин");
  if (password.get() === "") errors.push("Введите пароль");
  if (errors.length > 0) {
    setAlertMessage(errors.join("\n"));
    setShowAlert(true);
    return;
  }
  auth();
};

const registerNext = () => {
  const errors = [];
  if (login.get() === "") errors.push("Введите логин");
  if (login.get().length > 10)
    errors.push("Логин должен быть короче 10 символов");
  if (password.get() === "") errors.push("Введите пароль");
  if (password.get().length < 6)
    errors.push("Пароль должен быть длинее 5 символов");
  if (errors.length > 0) {
    setAlertMessage(errors.join("\n"));
    setShowAlert(true);
    return;
  }
  setShowSpinner(true);
  checkIfUserExists().then((exists) => {
    if (!exists) {
      setShowSpinner(false);
      setShowRegisterAgeAndGender(true);
    } else {
      setAlertMessage("Такое имя пользователя уже занято");
      setShowAlert(true);
      setShowSpinner(false);
    }
  });
};

export default {
  toggleComments,
  showAuth,
  setShowAuth,
  showRegisterAgeAndGender,
  setShowRegisterAgeAndGender,
  showSpinner,
  setShowSpinner,
  showAlert,
  setShowAlert,
  alertMessage,
  setAlertMessage,
  authorize,
  registerNext,
  showCreateHub,
  setShowCreateHub,
};
