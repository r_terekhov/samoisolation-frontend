import React, { useState, useEffect } from "react";
import { TransitionGroup } from "react-transition-group";
import Fade from "react-reveal/Fade";
import { Brand } from "./components/Brand/Brand";
import { Navbar } from "./components/Navbar/Navbar";
import { Feed } from "./components/Feed/Feed";
import { Login } from "./components/Login/Login";
import "./App.css";
import { Feedback } from "./components/Feedback/Feedback";
import { AddHubButton } from "./components/AddHubButton/AddHubButton";
import { AddHubForm } from "./components/AddHubForm/AddHubForm";
import { PostDetail } from "./components/PostDetail/PostDetail";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  useHistory,
} from "react-router-dom";
import { inject, observer } from "mobx-react";
import { Welcome } from "./components/Welcome/Welcome";

const App = inject("rootStore")(
  observer((props) => {
    const [toggle, setToggle] = useState(false);
    const [showFeedback, setShowFeedback] = useState(false);
    let history = useHistory();

    const toggleSidebar = () => setToggle(!toggle);

    const { uiStore } = props.rootStore;
    const showAuth = uiStore.showAuth.get();
    const showCreateHub = uiStore.showCreateHub.get();
    useEffect(() => {
      const isOld = JSON.parse(localStorage.getItem("isOld")) || false;
      if (!isOld) history.push("/welcome");
    }, []);
    return (
      <div className="App">
        <TransitionGroup appear enter exit>
          <Switch>
            <Route exact path="/category/:category">
              <div className="navigation position-absolute">
                <Brand toggleSidebar={toggleSidebar} toggle={toggle} />
                <Navbar
                  toggle={toggle}
                  toggleSidebar={toggleSidebar}
                  setShowFeedback={setShowFeedback}
                />
              </div>
              <div className="content">
                <div className="feed-wrapper">
                  <Feed />
                </div>
              </div>
              <AddHubButton />
              {showAuth && <Login />}
              {showCreateHub ? <AddHubForm /> : ""}
              {showFeedback && <Feedback setShowFeedback={setShowFeedback} />}
            </Route>

            <Route exact path="/">
              <div className="navigation position-absolute">
                <Brand toggleSidebar={toggleSidebar} toggle={toggle} />
                <Navbar
                  toggle={toggle}
                  toggleSidebar={toggleSidebar}
                  setShowFeedback={setShowFeedback}
                />
              </div>
              <div className="content">
                <div className="feed-wrapper">
                  <Feed />
                </div>
              </div>
              <AddHubButton />
              {showAuth && <Login />}
              {showCreateHub ? <AddHubForm /> : ""}
              {showFeedback && <Feedback setShowFeedback={setShowFeedback} />}
            </Route>

            <Route exact path="/post/:hubId">
              <div className="navigation position-absolute">
                <Brand toggleSidebar={toggleSidebar} toggle={toggle} />
                <Navbar
                  toggle={toggle}
                  toggleSidebar={toggleSidebar}
                  setShowFeedback={setShowFeedback}
                />
              </div>
              <div className="content">
                <div className="feed-wrapper">
                  <PostDetail />
                </div>
              </div>
              <AddHubButton />
              {showAuth && <Login />}
              {showCreateHub ? <AddHubForm /> : ""}
              {showFeedback && <Feedback setShowFeedback={setShowFeedback} />}
            </Route>
            <Route path="/welcome">
              <Welcome />
            </Route>
          </Switch>
        </TransitionGroup>
      </div>
    );
  })
);

export default App;
