import { useState, useEffect } from "react";
import { URL } from "../constants";
import axios from "axios";

export default function useInfiniteScroll(
  page,
  fetchFunction,
  dependencies,
  resetFunction,
  appendFunction = () => {},
  fetchFunctionParams = []
) {
  const [loading, setLoading] = useState(true);
  const [hasMore, setHasMore] = useState(false);

  useEffect(() => {
    setLoading(true);
    fetchFunction(page, ...fetchFunctionParams).then((data) => {
      if (data === undefined) return;
      if (data.length > 0) appendFunction(data);
      setHasMore(data.length > 0);
      setLoading(false);
    });
  }, [...dependencies]);

  useEffect(() => {
    if (page > 0) resetFunction();
  }, [dependencies[1]]);

  return { loading, hasMore };
}
