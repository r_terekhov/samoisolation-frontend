import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import "bootstrap/dist/css/bootstrap.min.css";
import App from "./App";
import { Provider } from "mobx-react";
import rootStore from "./store";
import { BrowserRouter as Router } from "react-router-dom";

ReactDOM.render(
  <Router>
    <Provider rootStore={rootStore}>
      <App />
    </Provider>
  </Router>,
  document.getElementById("root")
);
