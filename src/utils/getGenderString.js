export default (gender) => {
  switch (gender) {
    case 0:
      return "Ж";
    case 1:
    default:
      return "М";
  }
};
