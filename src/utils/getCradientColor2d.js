import getColorString from "./getColorString"

const gradientStart = [71, 121, 251];
const gradientEnd = [248, 77, 106];

export default (length, pos) => {
  if (length === 1) return getColorString(...gradientEnd);
  const diff = [];
  gradientStart.forEach((_, ind) => {
    diff.push(gradientEnd[ind] - gradientStart[ind]);
  });
  const step = diff.map((df) => {
    return df / length;
  });
  const rgb = gradientEnd.map((colorComponent, ind) =>
    (colorComponent - step[ind] * pos).toFixed(0)
  );
  return getColorString(...rgb);
};