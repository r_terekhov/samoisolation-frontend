import React, { useState } from "react";
import { inject, observer } from "mobx-react";
import InputRange from "react-input-range";
import { URL } from "../../constants";
import "react-input-range/lib/css/index.css";

export const Feedback = inject("rootStore")(
  observer((props) => {
    const [rate, setRate] = useState(4);
    const [feedback, setFeedback] = useState("");
    const [loading, setLoading] = useState(false);
    const [finished, setFinished] = useState(false);
    return (
      <div
        className="login-bg position-fixed d-flex align-items-center justify-content-center animated fadeIn faster"
        id="feedback-bg"
        onClick={(e) => {
          if (e.target.id === "feedback-bg") {
            props.setShowFeedback(false);
          }
        }}
      >
        <div className="auth p-4 p-md-5 feedback-container">
          <p className="my-2" style={{ fontSize: "18px", fontWeight: "500" }}>
            Оцени SamoIsolation
          </p>
          <div className="my-4 py-3 px-2">
            <InputRange
              maxValue={5}
              minValue={1}
              value={rate}
              onChange={(value) => setRate(value)}
              style={{ fontSixe: "24px", color: "white" }}
            />
          </div>
          <p
            className="my-2 mb-3"
            style={{ fontSize: "18px", fontWeight: "500" }}
          >
            Оставь нам комментарий
          </p>
          <textarea
            className="add-hub-text my-2 w-100 feedback-textarea"
            rows="8"
            value={feedback}
            placeholder="Напиши сюда всё что хочешь"
            onChange={(e) => setFeedback(e.target.value)}
          ></textarea>
          {loading && (
            <div className="d-flex justify-content-center my-2 mb-3 animated fadeIn faster">
              <div className="spinner-border text-light" role="status">
                <span className="sr-only">Loading...</span>
              </div>
            </div>
          )}
          {finished && (
            <div
              class="alert alert-light animated fadeIn faster"
              role="alert"
              style={{ color: "rgba(0,0,0,.8)" }}
            >
              Спасибо, что оставили отзыв! Мы этого не забудем 💪
            </div>
          )}
          <button
            type="submit"
            className="add-hub-submit p-2 my-2 w-100"
            onClick={(e) => {
              e.preventDefault();
              setLoading(true);
              fetch(`${URL}/core/feedback/giveFeedback`, {
                method: "POST",
                headers: {
                  "Content-Type": "application/json",
                },
                body: JSON.stringify({
                  rating: Number(rate),
                  content: feedback,
                }),
              })
                .then((response) => response.json())
                .then((response) => {
                  setLoading(false);
                  setFinished(true);
                  setTimeout(() => props.setShowFeedback(false), 2000);
                });
            }}
          >
            Отправить
          </button>
        </div>
      </div>
    );
  })
);
