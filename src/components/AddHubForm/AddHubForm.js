import React, { useState } from "react";
import { toJS } from "mobx";
import { inject, observer } from "mobx-react";
import { useHistory } from "react-router-dom";

export const AddHubForm = inject("rootStore")(
  observer((props) => {
    const { mediaStore, uiStore } = props.rootStore;
    const categories = toJS(mediaStore.categories);
    const selected = mediaStore.newHubCategory.get();
    const options = categories.map(({ category, name, emoji }) => (
      <option value={category}>{`${name} ${emoji}`}</option>
    ));
    const value = mediaStore.newHubContent.get();
    const history = useHistory();
    const [loading, setLoading] = useState(false);
    return (
      <div
        className="login-bg position-fixed d-flex align-items-center justify-content-center animated fadeIn faster"
        id="create-hub-bg"
        onClick={(e) => {
          if (e.target.id === "create-hub-bg") {
            uiStore.setShowCreateHub(false);
          }
        }}
      >
        <form className="add-hub-form d-flex flex-column p-5">
          <select
            class="add-hub-category my-2 p-2"
            value={selected}
            onChange={(e) => mediaStore.setNewHubCategory(e.target.value)}
          >
            {options}
          </select>
          <textarea
            className="add-hub-text my-2"
            rows="8"
            value={value}
            onChange={(e) => mediaStore.setNewHubContent(e.target.value)}
          ></textarea>
          {loading && (
            <div className="w-100 d-flex justify-content-center">
              <div className="spinner-border text-light" role="status">
                <span className="sr-only">Loading...</span>
              </div>
            </div>
          )}
          <button
            type="submit"
            className="add-hub-submit p-2 my-2"
            onClick={(e) => {
              e.preventDefault();
              if (toJS(mediaStore.categories).length !== 0) {
                setLoading(true);
                mediaStore.createPost().then(({ hubId }) => {
                  setLoading(false);
                  history.push(`/post/${hubId}`);
                });
              }
            }}
          >
            Создать
          </button>
        </form>
      </div>
    );
  })
);
