import React, { useEffect } from "react";
import { inject, observer } from "mobx-react";
import plus from "../../icons/plus.svg";

export const AddHubButton = inject("rootStore")(
  observer((props) => {
    const { uiStore, authStore } = props.rootStore;
    const showCreateHub = uiStore.showCreateHub.get();
    useEffect(() => {
      window.$(function () {
        window.$('[data-toggle="tooltip"]').tooltip();
      });
      window.$(document).ready(function () {
        window.$('[data-toggle="tooltip"]').click(function () {
          window.$('[data-toggle="tooltip"]').tooltip("hide");
        });
      });
    });
    return (
      <div
        className="add-button d-flex justify-content-center align-items-center p-4"
        data-toggle="tooltip"
        data-placement="left"
        title="Нажмите сюда, чтобы создать новую запись"
        onClick={(e) => {
          if (authStore.isLoggedIn.get())
            uiStore.setShowCreateHub(!showCreateHub);
          else uiStore.setShowAuth(true);
        }}
      >
        <img
          src="https://firebasestorage.googleapis.com/v0/b/samoisolation-free.appspot.com/o/plus.svg?alt=media&token=25667052-854c-471b-9f6b-021bbd40b72a"
          style={{ height: "19px" }}
        />
      </div>
    );
  })
);
