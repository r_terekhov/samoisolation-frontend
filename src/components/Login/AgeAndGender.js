import React from "react";
import { inject, observer } from "mobx-react";
import ToggleButton from "react-toggle-button";

const colors = {
  active: {
    base: "rgb(50,82,175)",
    hover: "rgb(43,70,150)",
  },
  inactive: {
    base: "rgb(50,82,175)",
    hover: "rgb(43,70,150)",
  },
};

const trackStyle = {
  boxShadow: "inset 0 1px 2px rgba(0,0,0,.35)",
};

const thumbStyle = {
  padding: "13px",
  boxShadow: "0 1px 4px rgba(0,0,0,.35)",
};

const activeLabelStyle = {
  fontFamily: "'Roboto', sans-serif",
  fontSize: "14px",
  color: "rgba(255,255,255,.8)",
};

const inactiveLabelStyle = {
  fontFamily: "'Roboto', sans-serif",
  fontSize: "14px",
  color: "rgba(255,255,255,.8)",
};

export const AgeAndGender = inject("rootStore")(
  observer((props) => {
    const { uiStore, authStore } = props.rootStore;

    const gender = authStore.gender.get();
    const age = authStore.age.get();

    return (
      <div className="d-flex flex-column">
        <label htmlFor="" className="w-100 text-left my-0 px-1">
          Возраст
        </label>
        <input
          type="number"
          min="1"
          className="login-login px-3 py-2 my-2"
          value={age}
          onChange={(e) => {
            if (Number(e.target.value) >= 0 && Number(e.target.value) <= 100) {
              authStore.setAge(e.target.value);
            }
          }}
        />
        <div className="d-flex my-2">
          <label htmlFor="" className="w-100 text-left my-0 px-1">
            Пол
          </label>
          <ToggleButton
            inactiveLabel="Ж"
            activeLabel="М"
            colors={colors}
            trackStyle={trackStyle}
            thumbStyle={thumbStyle}
            activeLabelStyle={activeLabelStyle}
            inactiveLabelStyle={inactiveLabelStyle}
            value={gender}
            onToggle={(value) => {
              authStore.setGender(!value);
            }}
          />
        </div>
        <button
          type="submit"
          className="login-button p-2 my-2"
          onClick={(e) => {
            e.preventDefault();
            uiStore.setShowRegisterAgeAndGender(false);
            window.ym(62050042, "reachGoal", "123123");
          }}
        >
          Назад
        </button>
        <button
          type="submit"
          className="login-button p-2 my-2"
          onClick={(e) => {
            e.preventDefault();
            authStore.register();
          }}
        >
          Зарегистрироваться
        </button>
      </div>
    );
  })
);
