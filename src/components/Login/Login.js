import React from "react";
import { inject, observer } from "mobx-react";
import { AgeAndGender } from "./AgeAndGender";

export const Login = inject("rootStore")(
  observer((props) => {
    const { uiStore, authStore } = props.rootStore;

    const login = authStore.login.get();
    const regPassword = authStore.password.get();

    const showRegisterAgeAndGender = uiStore.showRegisterAgeAndGender.get();
    const showSpinner = uiStore.showSpinner.get();
    const showAlert = uiStore.showAlert.get();
    const alertMessage = uiStore.alertMessage.get();

    return (
      <div
        className="login-bg position-fixed d-flex align-items-center justify-content-center animated fadeIn faster"
        id="login-bg"
        onClick={(e) => {
          if (e.target.id === "login-bg") uiStore.setShowAuth(false);
        }}
      >
        <div className="auth d-flex p-2">
          <form className="register p-4">
            {!showRegisterAgeAndGender ? (
              <div className="d-flex flex-column">
                {showAlert ? (
                  <div className="alert alert-danger" role="alert">
                    <p style={{ whiteSpace: "pre-line" }} className="mb-0">
                      {alertMessage}
                    </p>
                  </div>
                ) : (
                  ""
                )}
                <label htmlFor="" className="w-100 text-left my-0 px-1">
                  Логин
                </label>
                <input
                  type="text"
                  className="login-login px-3 py-2 my-2"
                  value={login}
                  onChange={(e) => authStore.setLogin(e.target.value)}
                />
                <label htmlFor="" className="w-100 text-left my-0 px-1">
                  Пароль
                </label>
                <input
                  type="password"
                  className="login-password px-3 py-2 my-2"
                  value={regPassword}
                  onChange={(e) => authStore.setPassword(e.target.value)}
                />
                {showSpinner ? (
                  <div className="d-flex justify-content-center">
                    <div
                      className="spinner-border text-danger my-2"
                      role="status"
                    >
                      <span className="sr-only">Loading...</span>
                    </div>
                  </div>
                ) : (
                  ""
                )}

                <button
                  type="submit"
                  className="login-button p-2 my-2"
                  onClick={(e) => {
                    e.preventDefault();
                    uiStore.authorize();
                  }}
                >
                  Войти
                </button>
                <button
                  type="submit"
                  className="login-button p-2 my-2"
                  onClick={(e) => {
                    e.preventDefault();
                    uiStore.registerNext();
                  }}
                >
                  Зарегистрироваться
                </button>
              </div>
            ) : (
              <AgeAndGender />
            )}
          </form>
        </div>
      </div>
    );
  })
);
