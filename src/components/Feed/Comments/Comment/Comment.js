import React from "react";
import getGenderString from "../../../../utils/getGenderString";

export function Comment(props) {
  const { message, authorNick, authorAge, authorGender } = props;
  return (
    <div className="d-flex flex-column align-items-start my-2 px-2">
      <span className="comment-author mb-2">{`${authorNick}, ${authorAge}, ${getGenderString(
        authorGender
      )}`}</span>
      <p
        className="comment-text py-3 px-3 px-md-4 text-left"
        style={{ whiteSpace: "pre-line" }}
      >
        {message}
      </p>
    </div>
  );
}
