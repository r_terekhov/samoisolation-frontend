import React, { useState, useEffect, useRef, useCallback } from "react";
import Fade from "react-reveal/Fade";
import { Comment } from "./Comment/Comment";
import { CommentInput } from "./CommentInput/CommentInput";
import useInfiniteScroll from "../../../hooks/useInfiniteScroll";
import { inject, observer } from "mobx-react";

export const Comments = inject("rootStore")(
  observer((props) => {
    const { hubId } = props;
    const { mediaStore } = props.rootStore;

    const [page, setPage] = useState(0);
    const [comments, setComments] = useState([]);

    const fetchFunction = mediaStore.fetchComments;
    const resetComments = () => {};
    const dependencies = [page];
    const appendComments = (comments) => {
      setComments((prevComments) => [...prevComments, ...comments]);
    };
    const params = [hubId];
    const { hasMore, loading } = useInfiniteScroll(
      page,
      fetchFunction,
      dependencies,
      resetComments,
      appendComments,
      params
    );

    const observer = useRef();
    const lastBookElementRef = useCallback(
      (node) => {
        if (loading) return;
        if (observer.current) observer.current.disconnect();
        observer.current = new IntersectionObserver((entries) => {
          if (entries[0].isIntersecting && hasMore) {
            setPage((prevPageNumber) => prevPageNumber + 1);
          }
        });
        if (node) observer.current.observe(node);
      },
      [loading, hasMore]
    );

    const content = comments.map(
      ({ content: message, authorNick, authorAge, authorGender }, index) => {
        if (comments.length === index + 1) {
          return (
            <div
              ref={lastBookElementRef}
              key={index}
              className="animated fadeInUp slow"
            >
              <Comment
                message={message}
                authorNick={authorNick}
                authorAge={authorAge}
                authorGender={authorGender}
                key={index}
              />
            </div>
          );
        } else
          return (
            <div key={index} className=" animated fadeInUp slow">
              <Comment
                message={message}
                authorNick={authorNick}
                authorAge={authorAge}
                authorGender={authorGender}
                key={index}
              />
            </div>
          );
      }
    );
    return (
      <div className="bg-white comments-container py-3 px-1 px-md-3">
        {content}
        {loading && (
          <div className="py-4">
            <div className="spinner-border text-dark" role="status">
              <span className="sr-only">Loading...</span>
            </div>
          </div>
        )}
        {!loading && content.length === 0 && (
          <div className="my-4 text-center">
            Здесь пока нет комментариев. Будь первым!
          </div>
        )}
        <CommentInput hubId={hubId} setComments={setComments} />
      </div>
    );
  })
);
