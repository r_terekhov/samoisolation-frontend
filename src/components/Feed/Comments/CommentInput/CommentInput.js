import React from "react";
import { inject, observer } from "mobx-react";

export const CommentInput = inject("rootStore")(
  observer((props) => {
    const { hubId } = props;
    const { mediaStore } = props.rootStore;
    const value = mediaStore.newCommentContent.get();
    return (
      <form className="comment-form py-2 px-4 mx-1">
        <div className="row">
          <div className="col-10 col-md-11 d-flex align-items-center justify-content-center border-right">
            <input
              type="text"
              placeholder="Оставьте комментарий..."
              value={value}
              className="comment-input w-100 p-2"
              onChange={(e) => mediaStore.setNewCommentContent(e.target.value)}
            />
          </div>
          <div className="col-2 col-md-1 d-flex align-items-center justify-content-center">
            <button
              type="submit"
              className="comment-send-wrapper"
              onClick={(e) => {
                e.preventDefault();
                mediaStore.createComment(hubId, props.setComments);
              }}
            >
              <img
                src="https://firebasestorage.googleapis.com/v0/b/samoisolation-free.appspot.com/o/send.svg?alt=media&token=aa891170-cb68-4f1d-89c4-08d42230e4eb"
                alt=""
                style={{ height: "19px" }}
              />
            </button>
          </div>
        </div>
      </form>
    );
  })
);
