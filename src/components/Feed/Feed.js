import React, { useState, useRef, useCallback, useEffect } from "react";
import useInfiniteScroll from "../../hooks/useInfiniteScroll";
import { CardWrapper } from "./CardWrapper/CardWrapper";
import { Card } from "./Card/Card";
import { CategoryBanner } from "./CategoryBanner/CategoryBanner";
import { Filter } from "../Filter/Filter";
import { inject, observer } from "mobx-react";
import { toJS, entries, when } from "mobx";
import { useParams } from "react-router-dom";
import "./Feed.css";
import { URL } from "../../constants";
import axios from "axios";

export const Feed = inject("rootStore")(
  observer((props) => {
    const { mediaStore } = props.rootStore;
    const { category = "all" } = useParams();
    const currentFilter = mediaStore.currentFilter.get();
    mediaStore.setCurrentCategory(category);
    mediaStore.setNewHubCategory(category);

    const currentCategory = toJS(mediaStore.categories).find(
      ({ category: cat }) => cat === category
    ) || {
      description:
        "Добро пожаловать на SamoIsolation!\nВ ленте, которая находится ниже, пользователи делятся своими мыслями.\nСлева - фильтры по темам, вы сможете выбрать интересные именно вам.\nЗарегистрируйтесь и присоединяйтесь, мы вас ждали!",
      url:
        "https://firebasestorage.googleapis.com/v0/b/samoisolation-free.appspot.com/o/kart.png?alt=media&token=ffca5555-ac6e-4169-9c96-b578f79ca864",
    };

    const [page, setPage] = useState(0);

    useEffect(() => {
      setPage(0);
      mediaStore.resetHubs();
    }, [category, currentFilter]);

    const fetchFunction =
      category === "all"
        ? mediaStore.fetchHubs
        : mediaStore.fetchHubsWithCategories;
    const resetHubs = mediaStore.resetHubs;
    const dependencies = [page, category, currentFilter];
    const appendHubs = mediaStore.appendHubs;
    const fetchFunctionParams = [
      props.rootStore.mediaStore.currentFilter.get(),
    ];
    const { hasMore, loading } = useInfiniteScroll(
      page,
      fetchFunction,
      dependencies,
      resetHubs,
      appendHubs,
      fetchFunctionParams
    );

    const observer = useRef();
    const lastBookElementRef = useCallback(
      (node) => {
        if (loading) return;
        if (observer.current) observer.current.disconnect();
        observer.current = new IntersectionObserver((entries) => {
          if (entries[0].isIntersecting && hasMore) {
            setPage((prevPageNumber) => prevPageNumber + 1);
          }
        });
        if (node) observer.current.observe(node);
      },
      [loading, hasMore]
    );

    const hubs = toJS(mediaStore.hubs);

    const content = hubs.map((hub, index) => {
      if (hubs.length === index + 1)
        return (
          <div ref={lastBookElementRef} key={hub.hubId}>
            <CardWrapper index={index} refLink={lastBookElementRef}>
              <Card
                hubId={hub.hubId}
                hub={hub}
                index={index}
                hubsNumber={hubs.length}
              />
            </CardWrapper>
          </div>
        );
      else
        return (
          <div key={hub.hubId}>
            <CardWrapper index={index}>
              <Card
                hubId={hub.hubId}
                hub={hub}
                index={index}
                hubsNumber={hubs.length}
              />
            </CardWrapper>
          </div>
        );
    });

    return (
      <div className="feed-container px-1 px-md-5 py-4">
        <div className="w-100">
          <CategoryBanner
            description={currentCategory.description}
            photo={currentCategory.url}
          />
        </div>
        {content.length !== 0 && <Filter />}
        <div className="w-100 py-2">{content}</div>
        {loading && (
          <div className="py-4">
            <div className="spinner-border text-dark" role="status">
              <span className="sr-only">Loading...</span>
            </div>
          </div>
        )}
        {!loading && !hasMore && (
          <div className="py-4 text-center">Больше ничего нет :(</div>
        )}
      </div>
    );
  })
);
