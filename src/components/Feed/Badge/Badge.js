import React from "react";
import { toJS } from "mobx";
import { inject, observer } from "mobx-react";
import { VKShareButton } from "react-share";
import heart1 from "../../../icons/heart1.svg";
import heart2 from "../../../icons/heart2.svg";
import message from "../../../icons/message.svg";
import vk from "../../../icons/vk.svg";

export const Badge = inject("rootStore")(
  observer((props) => {
    const { commentsCounter, likesCounter, hubId } = props;
    const { uiStore, authStore, mediaStore } = props.rootStore;
    const isLoggedIn = authStore.isLoggedIn.get();
    const notLiked =
      toJS(authStore.likes).find(({ hubId: id }) => hubId === id) === undefined;
    const { appended = 0 } =
      toJS(mediaStore.appendedComments).find(({ hubId: id }) => hubId === id) ||
      {};
    return (
      <div className="card-badge row px-2 py-1 disable-select d-flex justify-content-center">
        <div
          className="col-4 d-flex justify-content-around px-1 border-right noselect"
          style={{ cursor: "pointer" }}
          onClick={() => mediaStore.toggleComments(hubId)}
        >
          <div className="d-flex justify-content-center align-items-center">
            {commentsCounter + appended}
          </div>
          <div className="d-flex justify-content-center align-items-center">
            <img
              src="https://firebasestorage.googleapis.com/v0/b/samoisolation-free.appspot.com/o/message.svg?alt=media&token=5693c64c-6414-4a39-ac54-b10c0b425133"
              alt=""
              className="px-1 pr-3"
              style={{ height: "19px" }}
            />
          </div>
        </div>
        <div
          className="col-4 d-flex justify-content-around px-1 border-right noselect"
          style={{ cursor: "pointer" }}
          onClick={() => {
            if (isLoggedIn) {
              console.log("liked");
              authStore.toggleLike(hubId);
              mediaStore.like(hubId);
            } else {
              uiStore.setShowAuth(true);
            }
          }}
        >
          <div className="d-flex justify-content-center align-items-center">
            {notLiked ? likesCounter : likesCounter + 1}
          </div>
          <div className="d-flex justify-content-center align-items-center liked">
            {notLiked ? (
              <img
                src="https://firebasestorage.googleapis.com/v0/b/samoisolation-free.appspot.com/o/heart1.svg?alt=media&token=319073a8-6016-435f-a2a2-8c8d29194b88"
                alt=""
                className="px-1"
                style={{ height: "19px" }}
              />
            ) : (
              <img
                src="https://firebasestorage.googleapis.com/v0/b/samoisolation-free.appspot.com/o/heart2.svg?alt=media&token=194ed8e4-8d6d-46b7-adda-c66c7416a663"
                alt=""
                className="px-1"
                style={{ height: "19px" }}
              />
            )}
          </div>
        </div>
        <div className="col-2 d-flex justify-content-around px-1 ">
          <VKShareButton url={`${window.location.origin}/post/${hubId}`}>
            <img
              src="https://firebasestorage.googleapis.com/v0/b/samoisolation-free.appspot.com/o/vk.svg?alt=media&token=781a7b5d-e9c5-4193-82f0-5764f9e70e8e"
              alt=""
              className="pl-2"
              style={{ height: "19px" }}
            />
          </VKShareButton>
        </div>
      </div>
    );
  })
);
