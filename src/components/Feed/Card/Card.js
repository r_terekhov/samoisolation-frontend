import React, { useState } from "react";
import { toJS } from "mobx";
import { inject, observer } from "mobx-react";
import { Badge } from "../Badge/Badge";
import { Comments } from "../Comments/Comments";
import Fade from "react-reveal/Fade";
import Zoom from "react-reveal/Zoom";
import Roll from "react-reveal/Roll";
import Bounce from "react-reveal/Bounce";
import getGenderString from "../../../utils/getGenderString";
import getGradientColor2d from "../../../utils/getCradientColor2d";

export const Card = inject("rootStore")(
  observer((props) => {
    const { hubId, hub } = props;
    const { mediaStore } = props.rootStore;
    const {
      content,
      authorNick,
      authorAge,
      authorGender,
      comments,
      likesCounter,
      commentsCounter,
      showComments,
    } = hub;

    return (
      <div className="my-3 card-container-wrapper w-100" key={hubId}>
        <div className="card-container p-4 text-left position-relative w-100">
          <span className="author">{`${authorNick}, ${authorAge}, ${getGenderString(
            authorGender
          )}`}</span>
          <p className="card-text w-100" style={{ whiteSpace: "pre-line" }}>
            {content}
          </p>
          <Badge
            likesCounter={likesCounter}
            commentsCounter={commentsCounter}
            hubId={hubId}
          />
        </div>
        {showComments ? <Comments comments={comments} hubId={hubId} /> : ""}
      </div>
    );
  })
);
