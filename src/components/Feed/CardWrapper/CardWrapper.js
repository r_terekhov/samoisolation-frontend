import React from "react";
import Fade from "react-reveal";
import fire from "../../../icons/fire.svg";

export function CardWrapper(props) {
  const { index } = props;
  const { children } = props;
  return (
    <div className="card-wrapper row px-1 px-md-4 mx-0 w-100 d-flex justify-content-end position-relative animated fadeInRight slow">
      {index < 10 && (
        <img
          src="https://firebasestorage.googleapis.com/v0/b/samoisolation-free.appspot.com/o/fire.svg?alt=media&token=ea2b4580-3297-4c4e-94b7-ff0a1b8340d9"
          alt=""
          style={{ height: "50px" }}
          className="icon-hot d-none d-md-block"
        />
      )}
      <div className="col-md-11 col-12 px-1 px-md-3">{children}</div>
    </div>
  );
}
