import React from "react";
import Fade from "react-reveal";

export function CategoryBanner(props) {
  const { description, photo } = props;
  return (
    <div className="row px-4 mx-0 w-100 d-flex justify-content-end mb-3 animated fadeIn faster position-static">
      <div className="col-md-11 col-12 px-3">
        <div className="row category-banner mx-0 p-lg-2">
          <div className="col-12 col-lg-5 d-flex align-items-center justify-content-center">
            <img
              className="my-4 mx-2 rounded"
              src={photo}
              alt=""
              style={{
                width: "200px",
                height: "200px",
                objectFit: "cover",
                boxShadow: "0 4px 4px rgba(0,0,0,.25)",
              }}
            />
          </div>
          <div
            className="col-12 col-lg-7 d-flex align-items-center text-xl-left text-center p-md-3 p-1 py-3 font-weight-normal"
            style={{ whiteSpace: "pre-line" }}
          >
            {description}
          </div>
        </div>
      </div>
    </div>
  );
}
