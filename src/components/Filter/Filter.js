import React from "react";
import Fade from "react-reveal/Fade";
import { observer, inject } from "mobx-react";

export const Filter = inject("rootStore")(
  observer((props) => {
    const availableFilters = [
      ["newFirst", "Сначала новые"],
      ["smartLikesAndComment", "По рейтингу"],
    ];
    const currentFilter = props.rootStore.mediaStore.currentFilter.get();
    const options = availableFilters.map((filter) => (
      <option value={filter[0]}>{filter[1]}</option>
    ));
    return (
      <div className="w-100 row my-2 d-flex justify-content-start justify-content-md-end px-4 animated fadeIn faster">
        <div className="col-12 col-md-5 d-flex align-items-center px-0 mt-2">
          <div class="input-group">
            <div class="input-group-prepend">
              <label
                class="input-group-text"
                for="postsFilter"
                style={{ backgroundColor: "#F85E61", color: "white" }}
              >
                Фильтр
              </label>
            </div>
            <select
              class="custom-select"
              id="postsFilter"
              value={currentFilter}
              onChange={(e) =>
                props.rootStore.mediaStore.setCurrentFilter(e.target.value)
              }
            >
              {options}
            </select>
          </div>
        </div>
      </div>
    );
  })
);
