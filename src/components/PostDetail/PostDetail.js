import React, { useState, useEffect } from "react";
import axios from "axios";
import { URL } from "../../constants";
import { useParams } from "react-router-dom";
import { inject, observer } from "mobx-react";
import { toJS } from "mobx";
import { Card } from "../Feed/Card/Card";
import MetaTags from "react-meta-tags";

export const PostDetail = inject("rootStore")(
  observer((props) => {
    // 5e974528830cad79eaa5791f
    const { hubId = "0" } = useParams();
    const [empty, setEmpty] = useState(false);
    const [loading, setLoading] = useState(true);
    useEffect(() => {
      axios({
        method: "GET",
        url: `${URL}/core/hub/getHub`,
        params: { hubId },
      })
        .then((response) => response.data)
        .then(({ response }) => {
          if (!response) {
            setEmpty(true);
          } else {
            props.rootStore.mediaStore.setHubs([
              Object.assign({}, response, { showComments: false }),
            ]);
            setLoading(false);
          }
        });
      return () => props.rootStore.mediaStore.resetHubs();
    }, [hubId]);

    const content = toJS(props.rootStore.mediaStore.hubs).map((hub) => (
      <Card hub={hub} hubId={hub.hubId}></Card>
    ));

    return (
      <div className="feed-container d-flex flex-column align-items-center px-1 px-md-5 py-4">
        {loading && (
          <div className="h-100 d-flex align-items-center justify-content-center">
            <div className="spinner-border text-primary" role="status">
              <span className="sr-only">Loading...</span>
            </div>
          </div>
        )}
        {empty && (
          <div className="h-100 d-flex align-items-center justify-content-center">
            К сожалению, такого поста нет :(
          </div>
        )}
        <div className="w-75">{!loading && !empty && content}</div>
      </div>
    );
  })
);
