import React from "react";
import { Link } from "react-router-dom";
import "./Brand.css";
import icon from "../../icons/home.svg";
import menu from "../../icons/menu.svg";

export function Brand(props) {
  return (
    <div className="brand bg-white d-flex justify-content-center align-items-center">
      <div className="d-flex flex-row-reverse flex-md-row justify-content-around justify-content-md-center align-items-end w-100 mx-md-0">
        <img
          src="https://firebasestorage.googleapis.com/v0/b/samoisolation-free.appspot.com/o/menu.svg?alt=media&token=01616165-13f6-4783-be4b-bedeac9f9dc4"
          onClick={(e) => props.toggleSidebar()}
          alt="home"
          className="toggle-button toggle-button d-sm-none text-body mx-1"
        />

        <Link
          to="/"
          className="text-decoration-none text-body d-flex justify-content-center align-items-end"
          onClick={(e) => {
            if (props.toggle) props.toggleSidebar();
          }}
        >
          <img
            src="https://firebasestorage.googleapis.com/v0/b/samoisolation-free.appspot.com/o/home.svg?alt=media&token=f347de54-ff97-4e3e-bf65-5f93eeef34f7"
            alt="home"
            className="logo mx-1"
          />
          <div className="title d-flex flex-column justify-content-center align-items-start">
            <h1 className="mb-0">Samo</h1>
            <h1 className="mb-0">Isolation</h1>
          </div>
        </Link>
      </div>
    </div>
  );
}
