import React, { useState, useEffect } from "react";
import { inject, observer } from "mobx-react";
import axios from "axios";
import { URL } from "../../../constants";
import circle from "../../../icons/circle.svg";

export const OnlineCounter = inject("rootStore")(
  observer((props) => {
    const [onlineCounter, setOnlineCounter] = useState(0);
    useEffect(() => {
      axios({
        method: "GET",
        url: `${URL}/core/status/online`,
      })
        .then((response) => response.data)
        .then((response) => setOnlineCounter(response));
    }, []);

    return (
      <div className="online-counter-container w-100 align-items-center justify-content-center my-3">
        {/* <i className="fas fa-circle icon pr-2"></i> */}
        <img
          src="https://firebasestorage.googleapis.com/v0/b/samoisolation-free.appspot.com/o/circle.svg?alt=media&token=9a3f01fa-9a0c-495c-841f-fa2dcb768d06"
          style={{ height: "20px" }}
          className="pr-2 pb-1"
        />
        <span className="online-counter">{onlineCounter} online</span>
      </div>
    );
  })
);
