import React from "react";
import { inject, observer } from "mobx-react";
import { toJS } from "mobx";
import { NavLink } from "react-router-dom";
import { TransitionGroup } from "react-transition-group";
import Fade from "react-reveal/Fade";

export const CategoriesList = inject("rootStore")(
  observer((props) => {
    const { mediaStore } = props.rootStore;
    const categories = toJS(mediaStore.categories);
    const content = categories.map(({ name, emoji, category }) => (
      // <Fade key={category} left>
      <NavLink
        to={`/category/${category}`}
        className="text-decoration-none animated fadeIn faster"
        key={category}
        activeClassName="active"
        onClick={(e) => props.toggleSidebar()}
      >
        <div className="category d-flex align-items-center my-2">
          <p className="category-title text-left px-4 mb-0">
            {name} <span className="emoji">{emoji}</span>
          </p>
        </div>
      </NavLink>
      // </Fade>
    ));
    return <div className="categories-list my-4 w-100">{content}</div>;
  })
);
