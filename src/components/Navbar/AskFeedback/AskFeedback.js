import React from "react";

export function AskFeedback(props) {
  return (
    <div
      className="ask-feedback-container w-100 justify-content-center mt-4 noselect"
      style={{ cursor: "pointer" }}
      onClick={(e) => props.setShowFeedback(true)}
    >
      <p className="feedback-proposal text-left p-3">
        Понравился SamoIsolation? Отправь нам свои отзывы или пожелания, мы их
        все читаем{" "}
        <span className="emoji" role="img" aria-label="rock-emoji">
          🤘
        </span>
      </p>
    </div>
  );
}
