import React from "react";

export function About() {
  return (
    <div
      className="about-container w-100 align-items-center justify-content-center my-3"
      data-toggle="tooltip"
      data-placement="bottom"
      title="Да, мы как реддит, только про самоизоляцию"
    >
      <p className="about text-center" style={{ margin: "auto" }}>
        Made with passion and love in the heart of Siberia
      </p>
    </div>
  );
}
