import React, { useState, useEffect } from "react";
import { inject, observer } from "mobx-react";
import getGenderString from "../../../utils/getGenderString";
import Fade from "react-reveal/Fade";
import { useHistory } from "react-router-dom";

export const Logout = inject("rootStore")(
  observer((props) => {
    const isLoggedIn = props.rootStore.authStore.isLoggedIn.get();
    const [userInfo, setUserInfo] = useState({
      login: "",
      age: "",
      gender: "",
    });
    const [nullFromStorage, setNullFromStorage] = useState(false);
    useEffect(() => {
      if (isLoggedIn) {
        const fromStorage = JSON.parse(localStorage.getItem("user"));
        if (fromStorage !== null) setUserInfo(fromStorage);
        else setNullFromStorage(true);
      }
    }, [isLoggedIn]);
    const history = useHistory();
    return (
      <div className="w-100 my-3 p-2 row" style={{ margin: "auto" }}>
        {isLoggedIn && !nullFromStorage && (
          <Fade>
            <div
              className="author d-flex col-6 align-items-center justify-content-center font-weight-light text-truncate"
              style={{ maxWidth: "50%" }}
            >{`${userInfo.login}`}</div>
            <button
              type="button"
              className="btn px-4 col-6"
              style={{
                border: "1px solid rgba(0,0,0,.8)",
                backgroundColor: "white",
              }}
              onClick={(e) => {
                props.rootStore.authStore.logout();
                history.push("/");
              }}
            >
              Выйти
            </button>
          </Fade>
        )}
        {!isLoggedIn && (
          <div className="w-100">
            <Fade>
              <button
                type="button"
                className="btn px-4"
                style={{
                  border: "1px solid rgba(0,0,0,.8)",
                  backgroundColor: "white",
                }}
                onClick={(e) => {
                  props.rootStore.authStore.reauth();
                }}
              >
                Войти
              </button>
            </Fade>
          </div>
        )}
      </div>
    );
  })
);
