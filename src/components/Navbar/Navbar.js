import React from "react";
import "./Navbar.css";
import { CategoriesList } from "./CategoriesList/CategoriesList";
import { AskFeedback } from "./AskFeedback/AskFeedback";
import { OnlineCounter } from "./OnlineCounter/OnlineCounter";
import { Logout } from "./Logout/Logout";
import { About } from "./About/About";

export function Navbar(props) {
  return (
    <div
      className={`navbar-panel bg-white align-items-center ${
        props.toggle ? "toggle" : ""
      }`}
    >
      <CategoriesList toggleSidebar={props.toggleSidebar} />
      <div className="divider"></div>
      <AskFeedback setShowFeedback={props.setShowFeedback} />
      <Logout />
      <div className="divider"></div>
      <OnlineCounter />
      <div className="divider"></div>
      <About />
    </div>
  );
}
