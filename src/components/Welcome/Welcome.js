import React, { useState, useRef } from "react";
import Fade from "react-reveal/Fade";
import { useHistory } from "react-router-dom";
import "./Welcome.css";

export function Welcome() {
  const [showCode, setShowCode] = useState(false);
  const history = useHistory();
  return (
    <div className="welcome-container">
      <div
        className="row mx-1 animated fadeIn slow"
        style={{ height: "100vh", width: "100vw" }}
      >
        <div className="col-12 col-md-6 d-flex flex-column justify-content-center">
          <h1 className="mx-auto welcome-h1">Samoisolation - первый</h1>
          <h1 className="mx-auto welcome-h1">в интернете сервис</h1>
          <h1 className="mx-auto welcome-h1">для самоизоляции!</h1>
        </div>
        <div className="col-6 d-none d-md-flex align-items-center h-100">
          <img
            className="w-75 float-left"
            src="https://cdn.dribbble.com/users/1411165/screenshots/11012963/media/cc172233497847472908ea2edf54bac4.png"
            alt=""
          />
        </div>
      </div>
      <div
        style={{
          width: "100vw",
          backgroundColor: "#FE7D7D",
          color: "rgba(255,255,255,1)",
        }}
      >
        <div className="row mx-md-5 px-md-5 mx-1 px-1 py-5 animated fadeIn faster">
          <div className="col-12 my-2 px-1">
            <h2 className="mx-auto welcome-h2 text-center px-md-5 px-1">
              Мы помогаем людям проводить время дома и легче пережить это
              нелегкое время. Если вы знаете, чем заняться - можете поделиться
              этим с миром. Если не знаете - можете посмотреть подборки других
              людей.
            </h2>
          </div>
          <div className="col-12 col-md-6 mx-auto px-1">
            <img
              src="https://cdn.dribbble.com/users/2383283/screenshots/11080854/media/95ef18d98c904b72acbab7c942dc775c.png"
              className="w-100"
              alt=""
            />
          </div>
          <div className="col-12 my-2 px-1">
            <h2 className="mx-auto welcome-h2 text-center px-md-5 px-1">
              Делитесь с другими тем, что для вас важно. Ищите вдохновение в
              занятиях и историях других пользователей. Наслаждайтесь уютной
              атмосферой общения. Находите помощь и поддержку от людей, попавших
              в ту же ситуацию, что и вы.
            </h2>
          </div>
        </div>
      </div>
      <div
        style={{
          minHeight: "40vh",
          width: "100vw",
          backgroundColor: "#F2D8EF",
          color: "rgba(0,0,0,1)",
        }}
      >
        <div className="col-12 pt-5 px-1 animated fadeIn faster">
          <button
            className="mx-auto welcome-h2 text-center px-5 py-2 welcome-button"
            style={{
              border: "none",
              borderRadius: "20px",
              backgroundColor: "white",
              boxShadow: "0 4px 4px rgba(0,0,0,.25)",
              fontSize: "24px",
              color: "black",
            }}
            onClick={() => {
              setShowCode((prev) => !prev);
            }}
          >
            Кодекс домоседа
          </button>
        </div>
        {showCode && (
          <div className="col-12 py-5 px-2 px-md-5 animated fadeIn faster">
            <div className="container" id="code-body">
              <h2 className="mx-auto welcome-h2 text-left px-md-5 px-1">
                1. Домосед сидит дома и старается не нарушать режим
              </h2>
              <h2 className="mx-auto welcome-h2 text-left px-md-5 px-1">
                2. Домосед знает, что дом - это его крепость и главное место
                силы
              </h2>
              <h2 className="mx-auto welcome-h2 text-left px-md-5 px-1">
                3. Домосед старается не унывать и находить плюсы даже в
                самоизоляции
              </h2>
              <h2 className="mx-auto welcome-h2 text-left px-md-5 px-1">
                4. Домосед стремится максимально разнообразить свое
                времяпрепровождение
              </h2>
              <h2 className="mx-auto welcome-h2 text-left px-md-5 px-1">
                5. Домосед выходит в интернет, как в космос: исследует его и
                ищет новое
              </h2>
              <h2 className="mx-auto welcome-h2 text-left px-md-5 px-1">
                6. Домосед любит делиться с миром своей жизнью взаперти
              </h2>
              <h2 className="mx-auto welcome-h2 text-left px-md-5 px-1">
                7. Домосед - это призвание!
              </h2>
            </div>
          </div>
        )}
        {showCode && (
          <div className="col-12 py-2 pb-5 px-2 px-md-5 animated fadeIn faster">
            <button
              className="mx-auto welcome-h2 text-center px-5 py-2 welcome-button"
              style={{
                border: "none",
                borderRadius: "20px",
                backgroundColor: "white",
                fontSize: "22px",
                color: "black",
                boxShadow: "0 4px 4px rgba(0,0,0,.25)",
              }}
              onClick={() => history.push("/")}
            >
              Принять кодекс домоседа и продолжить
            </button>
          </div>
        )}
      </div>
    </div>
  );
}
